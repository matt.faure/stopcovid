# StopCovid

Suivi des faits et des opinions sur StopCovid

## 2021 Décembre

* 2021-12-10 NextINpact [TousAntiCovid : la CNIL déplore (encore) la tentation du « solutionnisme technologique »](https://www.nextinpact.com/article/49137/tousanticovid-cnil-deplore-encore-tentation-solutionnisme-technologique)
* 2021-12-02 NextInpact [Le gouvernement prive la CNIL des moyens d’évaluer l’efficacité du passe sanitaire](https://www.nextinpact.com/article/49039/le-gouvernement-prive-cnil-moyens-devaluer-lefficacite-passe-sanitaire)

## 2021 Juin 

* 2021-06-09 NextInpact [Pass sanitaire, données en clair, open source : « On fait au plus simple »](https://www.nextinpact.com/article/46147/pass-sanitaire-donnees-en-clair-open-source-on-fait-au-plus-simple)
* 2021-06-08 NextInpact [Pass sanitaire : la poudre aux yeux du pseudonymat, des données médicales en clair](https://www.nextinpact.com/article/46153/pass-sanitaire-poudre-aux-yeux-pseudonymat-donnees-medicales-en-clair)
* 2021-06-01 NextInpact [Covid-19 : les données médicales seront bien conservées pendant 20 ans](https://www.nextinpact.com/lebrief/47285/covid-19-donnees-medicales-seront-bien-conservees-pendant-20-ans)
  > Après avoir annoncé qu'elle ne seraient conservées que pendant 3 mois, afin de rassurer la population, le gouvernement avait depuis allongé cette durée à 20 ans

## 2021 Mai 

* 2021-05-21 NextInpact [L'ANSSI notifie 7 failles de sécurité Bluetooth](https://www.nextinpact.com/lebrief/47202/lanssi-notifie-7-failles-securite-bluetooth)
  > On ne saura donc pas, en l'état, si ces failles auraient été découvertes à l'occasion, ou en marge, des recherches effectuées par l'ANSSI autour de la sécurité de l'application TousAntiCovid.
* 2021-05-21 Le Monde [Contre le Covid-19, l’utilité des applications de traçage des cas contacts impossible à mesurer](https://www.lemonde.fr/pixels/article/2021/05/21/covid-19-l-impossible-mesure-de-l-utilite-des-applications-de-tracage-des-cas-contacts_6080953_4408996.html)

## 2021 Avril

* 2021-04-26 NextInpact [Le CCL-COVID publie un point de vigilance relatif au « pass sanitaire »](https://www.nextinpact.com/lebrief/46896/le-ccl-covid-publie-point-vigilance-relatif-au-pass-sanitaire)
* 2021-04-23 NextInpact [Carnet : la CNIL vigilante sur les ombres d’un pass sanitaire](https://www.nextinpact.com/lebrief/46877/tousanticovid)
* 2021-04-21 NextInpact [TousAntiCovid intègre un Carnet de tests, les vaccinations dès le 29 avril](https://www.nextinpact.com/article/45609/tousanticovid-integre-carnet-tests-vaccinations-des-29-avril)
* 2021-04-08 NextInpact [TousAntiCovid : 4,8 millions d’euros de dépenses de comm’](https://www.nextinpact.com/lebrief/46699/tousanticovid-48-millions-deuros-depenses-comm)

## 2021 Mars

* 2021-03-23 NextInpact [Application TousAntiCovid : Anticor saisit la Cour de justice de la République](https://www.nextinpact.com/lebrief/46536/application-stopcovid-anticor-saisit-cour-justice-republique)
  > \[Anticor] a décidé de porter plainte contre Olivier Véran, ministre de la Santé, pour favoritisme. 
* 2021-03-11 NextInpact [TousAntiCovid : le traçage par QR code se précise](https://www.nextinpact.com/lebrief/46407/tousanticovid-tracage-par-qr-code-se-precise)

## 2021 Février

* 2021-02-17 NextInpact [TousAntiCovid surveillera aussi les lieux visités via des QR Codes, les réserves de la CNIL](https://www.nextinpact.com/article/46045/tousanticovid-surveillera-aussi-lieux-visites-via-qr-codes-reserves-cnil)

## 2021 Janvier

* 2021-01-22 NextInpact [SI-DEP, Contact Covid, Vaccin COVID et TousAntiCovid : le bilan intermédiaire de la CNIL](https://www.nextinpact.com/lebrief/45698/si-dep-contact-covid-vaccin-covid-et-tousanticovid-bilan-intermediaire-cnil)
  > Des remarques ont été exprimées à l’égard de Contact COVID [...] En particulier [...] sur « certaines mauvaises pratiques résiduelles relatives aux conditions d’authentification, à la traçabilité et à la transmission de données personnelles à un tiers **non habilité à héberger des données de santé** ».
  > 
  > Sur TousAntiCovid [...] elle juge « indispensable de développer des initiatives et des indicateurs permettant **d’évaluer pleinement l’effectivité sanitaire du dispositif** dans le cadre de la lutte contre l’épidémie de COVID-19 ».
* 2021-01-21 NextInpact [Le fichier Contact Covid (significativement) étendu aux personnes « co-exposées »](https://www.nextinpact.com/lebrief/45591/le-fichier-contact-covid-significativement-etendu-aux-personnes-co-exposees)
* 2021-01-20 NextInpact [TousAntiCovid : un QR code dans les lieux ouverts au public et les moyens de transport](https://www.nextinpact.com/lebrief/45500/tousanticovid-qr-code-dans-lieux-ouverts-au-public-et-moyens-transport)

## 2020 Novembre

* 2020-11-25 Broken by Design [Attestation COVID-19 en ligne : un outil de surveillance globale sur Internet ?](https://www.broken-by-design.fr/posts/attestation-covid-19/)
  ... avec démonstration de comment organiser un suivi **nominatif** des utilisateurs de l'application.
* 2020-11-24 BFMTV [Les données de l'application de traçage australienne accidentellement collectées par une agence de renseignement](https://www.bfmtv.com/tech/les-donnees-de-l-application-de-tracage-australienne-accidentellement-collectees-par-une-agence-de-renseignement_AN-202011240242.html)
* 2020-11-04 NExtInpact [TousAntiCovid favorisée dans sa gestion de l'attestation, raté du QR Code, des évolutions peu transparentes](https://www.nextinpact.com/lebrief/44486/tousanticovid-favorise-dans-sa-gestion-attestation-rate-qr-code-evolutions-peu-transparentes)
  > Rien que pour le dernier commit effectué sur iOS, passant de la version 2.1.0 à 2.1.1, pas moins de 152 fichiers ont été modifiés. 511 pour la version 2.1.0 sous Android. Des pratiques qui vont à l'encontre des promesses de transparence du secrétaire d'État Cédric O.

## 2020 Octobre

* 2020-10-23 NextInpact [Quand la fragmentation d’Android pose des problèmes aux applications de contact tracing](https://www.nextinpact.com/lebrief/44311/quand-fragmentation-dandroid-pose-problemes-aux-applications-contact-tracing)
  > [...] activer la localisation depuis une app [...] implique de remonter des informations de localisation à Google.

## 2020 septembre

* 2020-09-24 NextInpact [StopCovid dans le flou, le gouvernement dans l’opacité](https://www.nextinpact.com/article/43840/stopcovid-dans-flou-gouvernement-dans-lopacite)
* 2020-09-15 Twitter Benjamin Bayard [Réserver un accès plus rapide aux tests Covid19 pour les utilisateurs de StopCovid](https://twitter.com/bayartb/status/1305931324243488772). À compléter de Twitter Alex Archambaud [Avis CNIL StopCovid](https://twitter.com/AlexArchambault/status/1305904431368753152)

## 2020 juillet

* 2020-07-21 NextInpact [Application StopCovid : La CNIL exige la correction de plusieurs irrégularités](https://www.nextinpact.com/news/109181-application-stopcovid-la-cnil-exige-correction-plusieurs-irregularites.htm)
* 2020-07-15 NextInpact [StopCovid : « l’utilité sanitaire concrète à ce jour semble bien négligeable »](https://www.nextinpact.com/brief/stopcovid-----l-utilite-sanitaire-concrete-a-ce-jour-semble-bien-negligeable---13091.htm)
  > seulement 14 utilisateurs avertis d'un risque de contact avec une personne contaminée, « StopCovid » apparaît bien comme un rendez-vous manqué.


  > Le rapport pointe du doigt le coût des serveurs (environ 200 000 euros par mois)

## 2020 juin

* 2020-06-24 NextInpact [40 000 euros d'hébergement et jusqu'à 80 000 euros de maintenance par mois pour StopCovid](https://www.nextinpact.com/brief/40-000-euros-d-hebergement-et-jusqu-a-80-000-euros-de-maintenance-par-mois-pour-stopcovid-12860.htm)
